# Changes
I have refactored the 'SearchStepDefinitions' class, 

>###fixed the following methods:
> he_Does_Not_See_The_Results
> 
> heSeesTheResultsDisplayedForMango
>###Added new methods:
>####heSeesTheResultsDisplayedForMangoIMPROVED()
> The improved version of 'heSeesTheResultsDisplayedForMango()', this one checks for multiple keywords, throws an error in case there are search results that do not match any of the input keywords.
> 
>####verifyNumberOfResults()
> Verifies the number of search results from the last api call
> 
I also added new models and utility classes to improve the existing architecture
>###Added models:
>####Product.java
>Object representing the search results
> 
>####Products List 
>Representing the search response
>###Added Utilities:
>####firstLetterCapped() 
>Capitalizes the first letter of a given string
>
>####checkProductTitlesForKeywords() 
>Verifies that the products in the search result contains certain keyword(s) this method works with a single keyword and multiple keywords alike.
>

# Changes
I have also refactored the execution. The app now executes with a maven plugin.
>In order to execute a specific feature file, add tags to the first line of your feature file & use:
>```
>mvn clean test -Dcucumber.options="--tags @Search"
>```