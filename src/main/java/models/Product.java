package models;

public class Product {
    String provider;
    String title;
    String url;
    String brand;
    Integer price;
    String unit;
    String isPromo;
    String promoDetails;
    String image;

    public Integer getPrice() {return price;}

    public String getBrand() {return brand;}

    public String getImage() {return image;}

    public String getIsPromo() {return isPromo;}

    public String getPromoDetails() {return promoDetails;}

    public String getProvider() {return provider;}

    public String getTitle() {return title;}

    public String getUnit() {return unit;}

    public String getUrl() {return url;}
}
