package utils;

import models.Product;
import models.Products;

public class Utilities {

    StringUtilities strUtils = new StringUtilities();

    public boolean checkProductTitlesForKeywords(String keyword, Products products){
        Products mismatched = new Products();
        String[] keywords = keyword.split(",");
        for (Product product:products){
            boolean matched = false;
            for (String key:keywords) {
                keyword = strUtils.firstLetterCapped(key.trim());
                matched = product.getTitle().contains(keyword) || product.getTitle().contains(keyword.toLowerCase());
                if (matched)
                    break;
            }
            if (!matched)
                mismatched.add(product);
        }
        if (mismatched.size()>0){
            for (Product product:mismatched)
                System.out.println("The product title \033[1;35m"+product.getTitle()+"\033[0m does not contain the keyword '"+keyword+"'");
            return false;
        }
        else return true;
    }
}
