package steps;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static org.assertj.core.api.Assertions.assertThat;
import net.thucydides.core.annotations.Steps;
import io.restassured.common.mapper.TypeRef;
import net.serenitybdd.rest.SerenityRest;
import static org.hamcrest.Matchers.*;
import io.cucumber.java.en.*;
import utils.Utilities;
import models.Products;
import models.Product;

public class SearchStepDefinitions {

    @Steps
    public CarsAPI carsAPI;

    @When("he calls endpoint {string}")
    public void heCallsEndpoint(String arg0) {
        SerenityRest.given().get(arg0);
    }

    @Then("he sees the results displayed for apple")
    public void heSeesTheResultsDisplayedForApple() {restAssuredThat(response -> response.statusCode(200));}

    @Then("he sees the results displayed for mango")
    public void heSeesTheResultsDisplayedForMango(){
        Products products = new Products(lastResponse().as(new TypeRef<>() {}));
        for (Product product:products) {assertThat(product.getTitle()).containsIgnoringCase("Mango");}
    }

    @Then("he does not see the results")
    public void he_Does_Not_See_The_Results() {
        restAssuredThat(response -> response.appendRootPath("detail").body("error", equalTo(true)));
    }

    @Then("he sees that there are {} results")
    public void verifyNumberOfResults(int expectedNumber){
        Products products = new Products(lastResponse().as(new TypeRef<>() {}));
        assertThat(products.size()).isEqualTo(expectedNumber);
    }

    @Then("he sees the results displayed for keyword(s) {}")
    public void heSeesTheResultsDisplayedForMangoIMPROVED(String keyword){
        Products products = new Products(lastResponse().as(new TypeRef<>() {}));
        Utilities utilities = new Utilities();
        assertThat(utilities.checkProductTitlesForKeywords(keyword,products)).isTrue();
    }
}
//This type of validation is not recommended, instead of failing the test with the first mismatch, the test should fail
//after all the mismatching titles are located. I have improved this validation my writing my own method above ^